﻿namespace Entities
{
    public class ResultSet
    {
        #region Properties

        public int SearchNumber { get; set; }
        public string FizzBuzz { get; set; }

        #endregion
    }
}
