﻿namespace Entities
{
    public class Pager
    {
        #region Properties

        public int PageSize { get; set; }
        public int PageNo { get; set; }
        public int TotalPages { get; set; }

        #endregion
    }
}
