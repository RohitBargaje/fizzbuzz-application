﻿namespace BusinessLayer
{
    class DivisibleByThreeAndFive : INumberRules
    {
        #region Public Method

        /// <summary>
        /// Get FizzBuzz string
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string FizzBuzzRule(int number)
        {
            if (number % 3 == 0 && number % 5 == 0)
            {
                return BusinessLayerConstant.FizzBuzz;
            }

            return number.ToString();
        }

        /// <summary>
        /// Check for rule
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public bool CheckRule(int number)
        {
            return (number % 3 == 0 && number % 5 == 0);
        }

        #endregion
    }
}