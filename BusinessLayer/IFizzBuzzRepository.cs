﻿namespace BusinessLayer
{
    public interface IFizzBuzzRepository
    {
        FizzBuzzModel GetFizzBuzzResultForGivenNumber(int number, int pageNo = 1);
    }
}
