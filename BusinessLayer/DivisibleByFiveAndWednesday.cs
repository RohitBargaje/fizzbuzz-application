﻿using System;

namespace BusinessLayer
{
    class DivisibleByFiveAndWednesday : INumberRules
    {
        #region Public Method

        /// <summary>
        /// Get FizzBuzz string
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string FizzBuzzRule(int number)
        {
            if (number % 5 == 0 && DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            {
                return BusinessLayerConstant.Wuzz;
            }

            return number.ToString();
        }

        /// <summary>
        /// Check for rule
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public bool CheckRule(int number)
        {
            return (number % 5 == 0 && DateTime.Now.DayOfWeek == DayOfWeek.Wednesday);
        }

        #endregion
    }
}