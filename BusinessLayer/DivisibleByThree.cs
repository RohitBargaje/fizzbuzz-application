﻿namespace BusinessLayer
{
    class DivisibleByThree : INumberRules
    {
        #region Public Method

        /// <summary>
        /// Get FizzBuzz string
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string FizzBuzzRule(int number)
        {
            if (number % 3 == 0)
            {
                return BusinessLayerConstant.Fizz;
            }

            return number.ToString();
        }

        /// <summary>
        /// Check for rule
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public bool CheckRule(int number)
        {
            return number % 3 == 0;
        }

        #endregion
    }
}
