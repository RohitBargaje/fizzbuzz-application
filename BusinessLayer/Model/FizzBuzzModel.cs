﻿using System.Collections.Generic;
using Entities;

namespace BusinessLayer
{
    public class FizzBuzzModel
    {
        #region Properties

        public int SearchNumber { get; set; }
        public List<ResultSet> ResultList { get; set; }
        public Pager Pager { get; set; }

        #endregion
    }
}
