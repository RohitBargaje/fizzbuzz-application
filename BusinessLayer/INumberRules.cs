﻿namespace BusinessLayer
{
    interface INumberRules
    {
        bool CheckRule(int number);
        string FizzBuzzRule(int number);
    }
}
