﻿namespace BusinessLayer
{
    interface IFizzBuzzRulesEngine
    {
        void BuildNumberRulesList(INumberRules numberRules);
        string ApplyRules(int number);
    }
}
