﻿using System.Collections.Generic;
using System.Linq;

namespace BusinessLayer
{
    class FizzBuzzRulesEngine : IFizzBuzzRulesEngine
    {
        #region Private Variables

        private readonly IList<INumberRules> lstNumberRules;

        #endregion

        #region Constructor

        public FizzBuzzRulesEngine()
        {
            lstNumberRules = new List<INumberRules>();
        }

        #endregion

        #region Public Method

        /// <summary>
        /// Create list of INumberRules
        /// </summary>
        /// <param name="numberRules"></param>
        public void BuildNumberRulesList(INumberRules numberRules)
        {
            if (numberRules != null)
            {
                lstNumberRules.Add(numberRules);
            }
        }

        /// <summary>
        /// Get FizzBuzz string as per number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string ApplyRules(int number)
        {
            var numberRule = lstNumberRules.FirstOrDefault(rule => rule.CheckRule(number));
            return numberRule != null ? numberRule.FizzBuzzRule(number) : number.ToString();
        }

        #endregion
    }
}
