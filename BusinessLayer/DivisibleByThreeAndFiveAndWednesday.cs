﻿using System;

namespace BusinessLayer
{
    class DivisibleByThreeAndFiveAndWednesday : INumberRules
    {
        #region Public Method

        /// <summary>
        /// Get FizzBuzz string
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string FizzBuzzRule(int number)
        {
            if (number % 3 == 0 && number % 5 == 0 && DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            {
                return BusinessLayerConstant.WizzWuzz;
            }

            return number.ToString();
        }

        /// <summary>
        /// Check for rule
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public bool CheckRule(int number)
        {
            return (number % 3 == 0 && number % 5 == 0 && DateTime.Now.DayOfWeek == DayOfWeek.Wednesday);
        }

        #endregion
    }
}
