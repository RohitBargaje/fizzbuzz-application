﻿using System;
using System.Collections.Generic;
using Entities;

namespace BusinessLayer
{
    public class FizzBuzzRepository : IFizzBuzzRepository
    {
        #region Public Method

        /// <summary>
        /// Get FizzBuzz result for given number
        /// </summary>
        /// <param name="number"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public FizzBuzzModel GetFizzBuzzResultForGivenNumber(int number, int pageNo = 1)
        {
            FizzBuzzModel fizzBuzzEntity = new FizzBuzzModel {SearchNumber = number};
            if (fizzBuzzEntity.ResultList == null)
            {
                fizzBuzzEntity.ResultList = new List<ResultSet>();
            }
            if (fizzBuzzEntity.Pager == null)
            {
                fizzBuzzEntity.Pager = new Pager
                {
                    PageNo = pageNo,
                    PageSize = BusinessLayerConstant.PageSize
                };
                var totalPages = (int)Math.Ceiling((Convert.ToDouble(number) / Convert.ToDouble(BusinessLayerConstant.PageSize)));
                fizzBuzzEntity.Pager.TotalPages = totalPages == 0 ? 1 : totalPages;
            }
            try
            {
                string strFizzBuzzResult = string.Empty;
                int pageSize = BusinessLayerConstant.PageSize;
                int startValue = (pageNo * pageSize) - (pageSize - 1);
                int endValue = number <= (pageNo * pageSize) ? number : (pageNo * pageSize);
                IFizzBuzzRulesEngine rulesEngine = new FizzBuzzRulesEngine();
                rulesEngine.BuildNumberRulesList(new DivisibleByThreeAndFiveAndWednesday());
                rulesEngine.BuildNumberRulesList(new DivisibleByThreeAndWednesday());
                rulesEngine.BuildNumberRulesList(new DivisibleByFiveAndWednesday());
                rulesEngine.BuildNumberRulesList(new DivisibleByThreeAndFive());
                rulesEngine.BuildNumberRulesList(new DivisibleByThree());
                rulesEngine.BuildNumberRulesList(new DivisibleByFive());

                for (int fizzBuzzNumber = startValue; fizzBuzzNumber <= endValue; fizzBuzzNumber++)
                {
                    strFizzBuzzResult = rulesEngine.ApplyRules(fizzBuzzNumber);
                    strFizzBuzzResult = strFizzBuzzResult.Trim();
                    fizzBuzzEntity.ResultList.Add(new ResultSet() { SearchNumber = fizzBuzzNumber, FizzBuzz = strFizzBuzzResult });
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("FizzBuzz calculation error - {0}", ex.InnerException));
            }
            return fizzBuzzEntity;
        }

        #endregion
    }
}
