﻿namespace BusinessLayer
{
    public static class BusinessLayerConstant
    {
        #region Constants

        public const string Fizz = "Fizz";
        public const string Buzz = "Buzz";
        public const string FizzBuzz = "FizzBuzz";
        public const string Wizz = "Wizz";
        public const string Wuzz = "Wuzz";
        public const string WizzWuzz = "WizzWuzz";
        public const int PageSize = 20;

        #endregion
    }
}
