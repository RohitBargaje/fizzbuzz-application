﻿using System.Web.Mvc;
using NUnit.Framework;
using FizzBuzz.Controllers;
using FizzBuzz.Models;
using FizzBuzz.Constant;
using BusinessLayer;
using NUnit.Mocks;

namespace FizzBuzzTest.Tests
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        #region Private Variables
        
        private DynamicMock fizzBuzzRepository;
        private FizzBuzzViewModel fizzBuzzModel;

        #endregion

        #region Test Method

        [SetUp]
        public void TestInit()
        {
            fizzBuzzRepository = new DynamicMock(typeof(IFizzBuzzRepository));
            fizzBuzzModel = new FizzBuzzViewModel
            {
                SearchNumber = 27,
                Pager = new Entities.Pager() {PageNo = 1, PageSize = BusinessLayerConstant.PageSize, TotalPages = 2}
            };
        }

        /// <summary>
        /// Test for view name result
        /// </summary>
        [Test]
        public void TestSearchResultViewName() 
        {
            fizzBuzzRepository.ExpectAndReturn("GetFizzBuzzResultForGivenNumber", fizzBuzzModel);
            var controller = new FizzBuzzController((IFizzBuzzRepository)fizzBuzzRepository.MockInstance);
            var result = controller.FizzBuzzResult(fizzBuzzModel) as RedirectToRouteResult;
            if (result != null) Assert.AreEqual(Constant.FizzBuzzResultView, result.RouteValues["action"]);
        }

        /// <summary>
        /// Test for record count
        /// </summary>
        [Test]
        public void TestSearchResultCount()
        {
            fizzBuzzRepository.ExpectAndReturn("GetFizzBuzzResultForGivenNumber", fizzBuzzModel);
            var controller = new FizzBuzzController((IFizzBuzzRepository)fizzBuzzRepository.MockInstance);
            var result = controller.FizzBuzzResult(fizzBuzzModel.Pager.PageNo, fizzBuzzModel.SearchNumber) as PartialViewResult;
            if (result != null)
            {
                var resultModel = (FizzBuzzViewModel)result.Model;
                var count = resultModel.ResultList.Count;
                Assert.AreEqual(20, count);
            }
        }

        #endregion
    }
}
