﻿using BusinessLayer;
using NUnit.Framework;

namespace BusinessLayerTest
{
    [TestFixture]
    public class TestBusinessLayer
    {
        #region Private Variables
        
        private readonly IFizzBuzzRepository fizzBuzzRepository;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public TestBusinessLayer()
        {
            fizzBuzzRepository = new FizzBuzzRepository();
        }

        #endregion

        #region Test Method

        /// <summary>
        /// Test for Fizz string
        /// </summary>
        [Test]
        public void TestSearchResultForFizz()
        {
            var result = fizzBuzzRepository.GetFizzBuzzResultForGivenNumber(27);
            var resultSet = result.ResultList[2];
            Assert.AreEqual(BusinessLayerConstant.Fizz, resultSet.FizzBuzz);
        }

        /// <summary>
        /// Test for Buzz string
        /// </summary>
        [Test]
        public void TestSearchResultForBuzz()
        {
            var result = fizzBuzzRepository.GetFizzBuzzResultForGivenNumber(27);
            var resultSet = result.ResultList[4];
            Assert.AreEqual(BusinessLayerConstant.Buzz, resultSet.FizzBuzz);
        }

        /// <summary>
        /// Test for FizzBuzz string
        /// </summary>
        [Test]        
        public void TestSearchResultForFizzBuzz()
        {
            var result = fizzBuzzRepository.GetFizzBuzzResultForGivenNumber(27);
            var resultSet = result.ResultList[14];
            Assert.AreEqual(BusinessLayerConstant.FizzBuzz, resultSet.FizzBuzz);
        }

        /// <summary>
        /// Test for Wizz string
        /// </summary>
        [Test]
        public void TestSearchResultForWizz()
        {
            var result = fizzBuzzRepository.GetFizzBuzzResultForGivenNumber(27);
            var resultSet = result.ResultList[2];
            Assert.AreEqual(BusinessLayerConstant.Wizz, resultSet.FizzBuzz);
        }

        /// <summary>
        /// Test for Wuzz string
        /// </summary>
        [Test]
        public void TestSearchResultForWuzz()
        {
            var result = fizzBuzzRepository.GetFizzBuzzResultForGivenNumber(27);
            var resultSet = result.ResultList[4];
            Assert.AreEqual(BusinessLayerConstant.Wuzz, resultSet.FizzBuzz);
        }

        /// <summary>
        /// Test for WizzWuzz string
        /// </summary>
        [Test]
        public void TestSearchResultForWizzWuzz()
        {
            var result = fizzBuzzRepository.GetFizzBuzzResultForGivenNumber(27);
            var resultSet = result.ResultList[14];
            Assert.AreEqual(BusinessLayerConstant.WizzWuzz, resultSet.FizzBuzz);
        }

        /// <summary>
        /// Test for total pages
        /// </summary>
        [Test]
        public void TestSearchResultForTotalPages()
        {
            var result = fizzBuzzRepository.GetFizzBuzzResultForGivenNumber(27);
            Assert.AreEqual(2, result.Pager.TotalPages);
        }

        #endregion
    }
}