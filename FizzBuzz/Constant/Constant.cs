﻿namespace FizzBuzz.Constant
{
    public static class Constant
    {
        #region Constants

        public const string FizzBuzzResultView = "FizzBuzzResult";

        #endregion
    }
}