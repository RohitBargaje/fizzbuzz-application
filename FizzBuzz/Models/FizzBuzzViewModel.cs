﻿using System.Collections.Generic;
using Entities;

namespace FizzBuzz.Models
{
    public class FizzBuzzViewModel
    {
        #region Properties

        public int SearchNumber { get; set; }
        public List<ResultSet> ResultList { get; set; }
        public Pager Pager { get; set; }

        #endregion
    } 
}