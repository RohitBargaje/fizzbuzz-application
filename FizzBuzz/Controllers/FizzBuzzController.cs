﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using BusinessLayer;
using FizzBuzz.Models;
using Entities;

namespace FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        #region Private Variables

        private readonly IFizzBuzzRepository fizzBuzzRepository;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for controller
        /// </summary>
        /// <param name="repository"></param>
        public FizzBuzzController(IFizzBuzzRepository repository) 
        {
            fizzBuzzRepository = repository;
        }

        #endregion

        #region Public Method

        /// <summary>
        /// Load serach panel.
        /// </summary>
        /// <returns></returns>
        public ActionResult Home()
        {
            var model = new FizzBuzzViewModel {SearchNumber = 1};
            return View(model);
        }

        /// <summary>
        /// Http get method for paging
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FizzBuzzResult(int pageNo, int number)
        {
            Mapper.CreateMap<FizzBuzzModel, FizzBuzzViewModel>();
            var fizzBuzzModel = fizzBuzzRepository.GetFizzBuzzResultForGivenNumber(number, pageNo);
            var fizzBuzzViewModel = Mapper.Map<FizzBuzzModel, FizzBuzzViewModel>(fizzBuzzModel);
            return PartialView(Constant.Constant.FizzBuzzResultView, fizzBuzzViewModel);
        }

        /// <summary>
        /// Http post method for filling web grid.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FizzBuzzResult(FizzBuzzViewModel model)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction(Constant.Constant.FizzBuzzResultView, new {pageNo = 1 , number = model.SearchNumber});
            }
            model.Pager = new Pager() { PageNo = 1 , PageSize = 1, TotalPages = 1};
            model.ResultList = new List<ResultSet>();
            return PartialView(Constant.Constant.FizzBuzzResultView, model);
        }

        #endregion        
    }
}
