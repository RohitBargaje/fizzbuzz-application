﻿using System.Web.Mvc;
using BusinessLayer;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;

namespace FizzBuzz
{
    public class Bootstrapper
    {
        #region Public Method

        /// <summary>
        /// Initialize method to get all dependencies.
        /// </summary>
        /// <returns></returns>
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            return container;
        }

        #endregion

        #region Private Method

        /// <summary>
        /// Register all dependencies.
        /// </summary>
        /// <returns></returns>
        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            container.RegisterType<IFizzBuzzRepository, FizzBuzzRepository>();
            return container;
        }

        #endregion
    }
}